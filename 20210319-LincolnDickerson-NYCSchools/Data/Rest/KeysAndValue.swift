//
//  SchoolKeysAndValue.swift
//  ChaseChallenge
//
//  Created by Link on 3/14/21.
//

struct HttpConstants {
    static let baseUrl = "https://data.cityofnewyork.us/resource"
    
    // In a full blown application there may be many of these endpoints or more likely dynamically created
    // but for these purposes they are hard coded
}

// This is included for completeness but not used in this project
struct HttpHeader {
    // Typically used to for puts, posts 
    static let applicationJson = "application/json; charset=UTF-8"
    static let contentType = "Content-Type"
    
    // typically these are used kinds of additional header information is used to supply access to
    // api data
    static let authorizationGroup = "Authorization"
    static let bearerAndToken = "Bearer typically_provided_by_the_data_supplier"
}
