//
//  SchoolsViewModel.swift
//  ChaseChallenge
//
//  Created by Link on 3/12/21.
//

import Foundation
import SwiftUI


class SchoolsViewModel: ObservableObject {
    @Published var allSchools = [SchoolModel]()
    @Published var count: Int = 0
    @Published var showWaitSpinner = false
    
    private let schoolDirectoryEndpoint = "/s3k6-pzi2"
    
    fileprivate let restManager = RestManager()
    fileprivate let baseUrl = URL(string: HttpConstants.baseUrl)
    fileprivate let decoder = JSONDecoder()
}

extension SchoolsViewModel {
    func getSchools() {
        get(schoolDirectoryEndpoint)
    }
    
    func responseHandler(_ results :RestManager.Results) {
        DispatchQueue.main.async { [self] in
            showWaitSpinner = false
            guard let response = results.response else { return }
            
            switch response.httpStatusCode {
            case 200: fallthrough
            case 201:
                guard let data = results.data else { return }
                do {
                    allSchools = try JSONDecoder().decode([SchoolModel].self, from: data)
                    allSchools.sort{ $0.school_name! < $1.school_name! }
                    
                } catch let DecodingError.dataCorrupted(context) {
                    print(context)
                } catch let DecodingError.keyNotFound(key, context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.valueNotFound(value, context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.typeMismatch(type, context)  {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    print("error: ", error)
                }
            default:
                print("\(response.httpStatusCode)")
                return
            }
        }
    }
    
    func get(_ subUrl: String = "") {
        guard let url = URL(string: "\(HttpConstants.baseUrl)\(subUrl)" ) else { return }
        
        restManager.requestHttpHeaders.clearValues()
        
        restManager.makeRequest(toURL: url, withHttpMethod: .get) { [self] (results) in
            DispatchQueue.main.async { showWaitSpinner = true }
            responseHandler(results)
        }
    }
    
    func put(_ subUrl: String) {
        guard let url = URL(string: "\(HttpConstants.baseUrl)\(subUrl)" ) else { return }
        
        restManager.requestHttpHeaders.clearValues()
        restManager.requestHttpHeaders.add( value: HttpHeader.bearerAndToken , forKey: HttpHeader.authorizationGroup )
        restManager.requestHttpHeaders.add( value: HttpHeader.applicationJson , forKey: HttpHeader.contentType )
        
        restManager.makeRequest(toURL: url, withHttpMethod: .put) { [self] (results) in
            showWaitSpinner = true
            responseHandler(results)
        }
    }
    
    // Not need for this application but include to express a complete Restful implementation
    func post(_ subUrl: String = "") {
        guard var url = baseUrl else { return }
        if !subUrl.isEmpty {
            guard let newUrl = URL(string: "\(HttpConstants.baseUrl)\(subUrl)" ) else { return }
            url = newUrl
        }
        
        restManager.requestHttpHeaders.clearValues()
        restManager.requestHttpHeaders.add( value: HttpHeader.bearerAndToken,  forKey: HttpHeader.authorizationGroup )
        restManager.requestHttpHeaders.add( value: HttpHeader.applicationJson, forKey: HttpHeader.contentType )
        
        restManager.makeRequest(toURL: url, withHttpMethod: .post) { [self] (results) in
            DispatchQueue.main.async { showWaitSpinner = true }
            responseHandler(results)
        }
    }
    
    
    // Not need for this application but include to express a complete Restful implementation
    func delete(_ subUrl: String = "") {
        guard let url = URL(string: "\(HttpConstants.baseUrl)\(subUrl)" ) else { return }
        
        restManager.requestHttpHeaders.clearValues()
        restManager.requestHttpHeaders.add( value: HttpHeader.bearerAndToken , forKey: HttpHeader.authorizationGroup )
        
        
        restManager.makeRequest(toURL: url, withHttpMethod: .delete) { (results) in
            guard let response = results.response else { return }
            if response.httpStatusCode == 200 {
                DispatchQueue.main.async {
                    // Typically this would take some sort of app appropiate action
                    print("Delete Complete")
                }
            } else {
                DispatchQueue.main.async {
                    // Appropriate action would be determind and handle here in a full implementation
                    print( "Delete Failure - \(response.httpStatusCode) : \(String(describing: response.response))")
                }
            }
        }
    }
}

