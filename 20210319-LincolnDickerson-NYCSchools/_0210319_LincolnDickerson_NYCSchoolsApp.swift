//
//  _0210319_LincolnDickerson_NYCSchoolsApp.swift
//  20210319-LincolnDickerson-NYCSchools
//
//  Created by Link on 3/19/21.
//

import SwiftUI

@main
struct _0210319_LincolnDickerson_NYCSchoolsApp: App {
    
    var schoolsVM = SchoolsViewModel()
    
    init() {
        schoolsVM.getSchools()
    }
    
    var body: some Scene {
        WindowGroup {
            AllSchoolsView()
                .environmentObject(schoolsVM)
        }
    }
}
