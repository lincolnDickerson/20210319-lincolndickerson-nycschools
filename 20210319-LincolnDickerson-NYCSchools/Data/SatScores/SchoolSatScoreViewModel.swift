//
//  SchoolSatScoreViewModel.swift
//  ChaseChallenge
//
//  Created by Link on 3/14/21.
//

import SwiftUI

class SchoolSatViewModel: ObservableObject {
    @Published var schoolScoreModel: SchoolSatScoreModel?
    @Published var showWaitSpinner = false
    
    private let satScoresEndpoint = "/f9bf-2cp4.json?dbn="
    
    fileprivate let restManager = RestManager()
    fileprivate let baseUrl = URL(string: HttpConstants.baseUrl)
    fileprivate let decoder = JSONDecoder()
}

extension SchoolSatViewModel {
    // Example of full Restful implementation can seen in SchoolsViewModel therefore
    // not expounding redundently of what could be done here
    // With more time though I believe a tighter solution could be found for
    // a generic response handler that took a closer for the details
    // so the HTTP response code would not be exist in two places.
    
    func getSatScoresbyDbn(_ dbn: String ) {
        get(satScoresEndpoint + dbn )
    }
    
    private func get(_ subUrl: String = "") {
        guard let url = URL(string: "\(HttpConstants.baseUrl)\(subUrl)" ) else { return }
        
        restManager.requestHttpHeaders.clearValues()
        restManager.makeRequest(toURL: url, withHttpMethod: .get) { [self] (results) in
            DispatchQueue.main.async {
                showWaitSpinner = true
                responseHandler(results)
            }
        }
    }
    
    private func responseHandler(_ results :RestManager.Results) {
        DispatchQueue.main.async { [self] in
            showWaitSpinner = false
            guard let response = results.response else { return }
            
            switch response.httpStatusCode {
            case 200: fallthrough
            case 201:
                guard let data = results.data else { return }
                do {
                    let allScores = try JSONDecoder().decode([SchoolSatScoreModel].self, from: data)
                    self.schoolScoreModel = allScores.first
                } catch let DecodingError.dataCorrupted(context) {
                    print(context)
                } catch let DecodingError.keyNotFound(key, context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.valueNotFound(value, context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch let DecodingError.typeMismatch(type, context)  {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    print("error: ", error)
                }
            default:
                print("\(response.httpStatusCode)")
                return
            }
        }
    }
}
