//
//  AllSchoolsView.swift
//  ChaseChallenge
//
//  Created by Link on 3/11/21.
//

import SwiftUI

struct AllSchoolsView: View {
    @EnvironmentObject var schoolsVM: SchoolsViewModel
    
    var body: some View {
        ZStack {
            Color.blue
            NavigationView {
                List {
                    // Implying there could me multiple sections
                    Section(header: Text("New York City")) {
                        ForEach(schoolsVM.allSchools, id: \.self) { school in
                            NavigationLink(
                                destination: DisplayScores(
                                    schoolName: school.school_name ?? "No School Name",
                                    dbn: school.dbn ?? "No DBN")
                            ) { Text( school.school_name!)}
                        }
                    }
                }.navigationTitle("Schools")
            }.navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AllSchoolsView().environmentObject( SchoolsViewModel() )
    }
}
