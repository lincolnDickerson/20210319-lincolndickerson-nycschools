//
//  DisplayScores.swift
//  ChaseChallenge
//
//  Created by Link on 3/19/21.
//

import SwiftUI

struct DisplayScores: View {
    @StateObject var scores = SchoolSatViewModel()
    
    let schoolName: String
    let dbn: String
    
    enum Details: Int, CaseIterable {
        case reading, math, writing
        
        var key: String {
            switch self {
            case .reading: return "Reading"
            case .math:    return "Math"
            case .writing: return "Writing"
            }
        }
    }
    
    var isScoreDataAvailable: Bool {
        get {
            scores.schoolScoreModel?.dbn != nil
        }
    }
    
    let layout = [
        GridItem(.fixed(40), spacing: 10),
        GridItem(.fixed(40), spacing: 10),
        GridItem(.fixed(40), spacing: 10)
    ]
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.gray.ignoresSafeArea(.all)
                VStack {
                    HStack {
                        Text(scores.schoolScoreModel?.schoolName ?? schoolName)
                            .font(.largeTitle)
                            .foregroundColor(.black)
                            .frame(width: geometry.size.width * 0.8, alignment: .leading)
                            .padding()
                    }
                    .padding(.bottom, 20)
                    LazyHGrid(rows: layout, spacing: 10) {
                        ForEach(Details.allCases, id: \.self ) { detail  in
                            Group {
                                switch detail {
                                case .reading:
                                    DetailCard(
                                        scores: scores.schoolScoreModel,
                                        detail: .reading)
                                        .frame(maxWidth: geometry.size.width * 0.30 )
                                    
                                case .math:
                                    DetailCard(
                                        scores: scores.schoolScoreModel,
                                        detail: .math)
                                        .frame(maxWidth: geometry.size.width * 0.30 )
                                case .writing:
                                    DetailCard(
                                        scores: scores.schoolScoreModel,
                                        detail: .writing)
                                        .frame(maxWidth: geometry.size.width * 0.30 )
                                }
                            }.font(.title)
                            Spacer()
                        }
                    }
                    Spacer()
                }
            }
            .onAppear() {
                scores.getSatScoresbyDbn(dbn)
            }
        }
    }
}

struct DisplayScores_Previews: PreviewProvider {
    @State static var scores = SchoolSatViewModel()
    static var previews: some View {
        DisplayScores(
            schoolName: "no school", dbn: "12345")
    }
}
