//
//  DetailCard.swift
//  ChaseChallenge
//
//  Created by Link on 3/19/21.
//

import SwiftUI

struct DetailCard: View {
    let scores: SchoolSatScoreModel?
    let detail: DisplayScores.Details
    
    enum ScoreDisplayType {
        case
            unavailable, notEntered, invalid
        
        var text: String {
            switch self {
            case .unavailable: return "Unavailable"
            case .notEntered:  return "No Score Entered"
            case .invalid:     return "Invalid Score"
            }
        }
        
        var font: Font {
            switch self {
            // debated using .default here ... since it was auto completed by the compiler
            // I left them as fallthroughs in case I wanted to fine tune later
            case .unavailable: fallthrough
            case .notEntered:  fallthrough
            case .invalid:     return Font.subheadline
            }
        }
    }
    
    private func displayScore(_ string: String? ) -> Text {
        // This is a demonstation of handling the different situation types of
        // data issues that can arise. In a more complex UI the user might be directed to
        // contact the school using the school information available
        guard let score = string else {
            let display = ScoreDisplayType.unavailable
            return Text(display.text).font(display.font)
        }
        guard !score.isEmpty else {
            let display = ScoreDisplayType.notEntered
            return Text(display.text).font(display.font)
        }
        guard Int(score) != nil else {
            let display: ScoreDisplayType = .invalid
            return Text(display.text).font(display.font)
        }
        return Text(score).font(.title)
    }
    
    var body: some View {
            Text(detail.key)
                .font(.title)
            switch detail {
            case .reading:
                displayScore( scores?.satCriticalReadingAvgScore )
            case .math:
                displayScore( scores?.satMathAvgScore )
            case .writing:
                displayScore( scores?.satWritingAvgScore )
            }
    }
}
